#!/usr/bin/env ruby

require 'fileutils'

# Globals
PLATFORM = `uname`
EM_HOME = "#{ENV["HOME"]}/.emacs.d"

# Check if emacs application exists and use it as global
if Dir.exist?("/Applications/Emacs.app")
  EMACS = "/Applications/Emacs.app/Contents/MacOS/Emacs".freeze
else
  EMACS = "emacs".freeze
end

# Deletes all files related to emacs installations
def clean_install
  if Dir.exist?("#{EM_HOME}/elpa")
  FileUtils.rm_rf "#{EM_HOME}/elpa/"
  end

  if File.exist?("#{EM_HOME}/custom.el")
    File.delete("#{EM_HOME}/custom.el")
  end

  if File.exist?("#{EM_HOME}/custom.el~")
    File.delete("#{EM_HOME}/custom.el~")
  end

  puts "Cleaned out emacs directory."
end

# Runs emacs to install packages. Instantly closes afterward
def install_emc
  if File.exist?("#{EM_HOME}/init.el")
    system EMACS, "-q", "-nw", "--script", "#{EM_HOME}/init.el"
  end
end

# Runs twice in case of custom.el issue
def init_emc
  FileUtils.touch("#{EM_HOME}/custom.el") unless File.exist?("#{EM_HOME}/custom.el")

  install_emc()

  Dir.mkdir("#{EM_HOME}/snippets") unless File.exist?("#{EM_HOME}/snippets")

  puts "Installed configuration."
end

# Updates config.org
# TODO: Add try/catch because this is BAD
def git_pull
  system "git", "pull"
  install_emc()
end

# Print statement
def param_listing
  puts "\tinit\tinstalls the emacs configuration"
  puts "\tclean\tremoves all packages and custom.el"
end

if ARGV.length < 1
  puts "No parameters found. Here is a list of parameters: \n"
  param_listing()
elsif ARGV.length > 2
  puts "Too many arguments found."
elsif ARGV[0] == "init"
  clean_install()
  init_emc()
  puts "Done!"
elsif ARGV[0] == "clean"
  clean_install()
  puts "Done!"
elsif ARGV[0] == "update"
  git_pull()
  install_emc()
else
  puts "Unknown parameter. Here is a list of parameters: \n"
  param_listing
end
