# My Emacs Configuration

### Installation
In order to install this configuration, `cd` into the `~/.emacs.d` directory and run: 
```bash
bin/emc init
```

